# BreadMoji

![example flushed](./examples/bread_flushed_face.png) ![example hot](./examples/bread_hot_face.png) ![example pleading](./examples/bread_pleading_face.png) ![example pensive](./examples/bread_pensive_face.png)

Automatically transposing the faces of Twemojis onto the bread Twemoji, creating BreadMojis.

Do with this whatever you want.

## Usage

Use linux, it wont complain about libcairo-2.dll.

- Clone the repo
- Get the twemoji submodule with `git submodule init` and `git submodule update`
- Install the requirements with pip
- Run `python3 bread.py file emojis.csv` or another command
- Enjoy images of bread in the "out" directory.

### More emojis

Add the names of the emoji you want to the emojis.csv file.
Use the "CLDR Short Name" from https://unicode.org/emoji/charts/full-emoji-list.html and replace the spaces and dashes with underscores.

Not all emojis will work (e.g. face_with_crossed_out_eyes does not seem to work) due to the `emoji` library not liking it. 

## Known issues 

- Windows needs libcairo-2.dll, good luck finding a reliable source for yourself, or just use WSL.
- Output SVGs are broken, and the resolution of the PNGs is locked
    - The SVGs seem to be a bit wonky with positioning etc. Currently the output CSVs are worthless, and the resolution can not be increased.
    - The current setup works with the library used to export to PNG, which is good enough for me.
- Some emojis suck, and need some manual finetuning (in emojis.csv)
- The code sucks
    - I have never really used python before, sorry!
