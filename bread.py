import svgutils.transform as sg
from svgutils.transform import fromfile
from svgutils.templates import VerticalLayout, ColumnLayout
import cairosvg
import emoji
import os
import csv
import sys

scale = 4

# Loads the SVG from twemoji (submodule) into an object from the svgutils lib.
def load_twemoji(slug):
    e = emoji.emojize(":" + slug + ":", language='alias')
    #print("Raw emoji:", e)
    e = ord(e)
    e = f"{e:x}"
    #print("Emoji", slug, "hex:", e)
    svg = fromfile("./twemoji/assets/svg/" + e + ".svg")
    return svg

# Create a bread emoji from the input.
# slug should be the name of an emoji
def breadify(slug, offset_y = str(0.35 * scale) + "cm", offset_x = str(0.07 * scale) + "cm"):
    fig = sg.SVGFigure("16cm", "16cm")
    bread = load_twemoji("bread")
    bread_outer = bread.root[0]
    bread_shade = bread.root[1]
    bread_inner = bread.root[2]
    bread_inner_initial_color = bread_inner.get("fill")
    fig.append(bread)

    e = load_twemoji(slug)
    e.set_size([str(2 * scale) + "cm", str(2 * scale) + "cm"])

    # svgutils is too limited/I dont know how to use it properly, so manual XML traversal time!
    root = e.root;
    root.set("y", offset_y)
    root.set("x", offset_x)
    bread_inner_color = None
    for element in root:
        color = element.get("fill")

        # Standard yellow colors of the emoji circle (face)
        # Remove that circle background color. 
        if color == "#FFAC33" or color == "#FFCC4D" or color == "#FFCB4C":
            element.set("fill", "")
            bread_inner_color = bread_inner_initial_color # Standard color, the light part of the bread.
        elif bread_inner_color == None: # Special case. An emoji where the main shape (circle) is not yellow!
            # Find the color of the main circle, so we can recolor the bread
            bread_inner_color = color
            element.set("fill", "")
        elif color == "#C1694F" or color == "#662113":
            # Disguised face has some weird (new?) colors around the nose, we don't want those. Ghetto fixes go brrrr
            element.set("fill", "")

    if bread_inner_color is not bread_inner_initial_color:
        print("Coloring bread:", bread_inner_color)    
        bread_inner.set("fill", bread_inner_color)

    fig.append(e)
    out_name = "out/bread_" + slug;
    out_name_svg = out_name + ".svg";
    out_name_png = out_name + ".png";

    if not os.path.exists("out"):
      os.mkdir("out")

    fig.save(out_name_svg);
    #print("Generated breadified svg on", out_name_svg)
    
    size = 128 * scale
    cairosvg.svg2png(url=out_name_svg, write_to=out_name_png, parent_width=size, parent_height=size)
    print("Generated breadified png on", out_name_png)

def create_breadify_args(row):
    # Depending on CSV row columns, we want different arguments, and to preserve the default option when one isnt given.
    # I am bad at python, so this is probs a stupid solution.
    args = {"slug": row[0]}
    if len(row) >= 2:
        args["offset_y"] = row[1]
    if len(row) >= 3:
        args["offset_x"] = row[2]

    return args

# Breadify an entire CSV file worth of emojis
# CSV should be formatted as follows:
# <Emoji Name>,<y offset>,<x offset>
# Comments in the csv can start with #
def breadify_csv(file_name): 
    print("Starting to process csv", file_name)

    with open(file_name) as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for row in reader:
            name = row[0]
            if name.startswith("#"):
                print("Skipping line", name)
                continue
            
            args = create_breadify_args(row)
            breadify(**args)


# Remove the first arg, which is bread.py
args = sys.argv[1:]

def display_usage():
    print("-=-=- Usage -=-=-")
    base = "python3 bread.py"
    print(base, "file <file>.csv")
    print(base, "emoji <emoji name> [offset_x] [offset_y]")
    print("   ")
    print("Used:", args)

if len(args) < 2:
    display_usage()
elif args[0] == "file":
    breadify_csv(args[1])
elif args[0] == "emoji":
    margs = create_breadify_args(args[1:])
    print("Calling with args", margs)
    breadify(**margs)
else:
    display_usage()
